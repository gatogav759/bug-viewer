#!/bin/bash
ls -la scanner/tmp/result/

command="curl -f -k --location '$SERVER/api/repository'"
command="$command --form 'token=\"$TOKEN\"'"
command="$command --form 'name=\"$REPOSITORY\"'"
command="$command --form 'ref=\"$REF\"'"
command="$command --form 'removePath=\"$(pwd)\"'"

if [ ! -z "$MODIFIED_FILES" ]; then
    for file in $MODIFIED_FILES; do
        command="$command --form 'modifiedFiles=$file'"
    done
fi

for file in scanner/tmp/result/*; do
    if [ -f "$file" ]; then
        command="$command --form 'files=@\"$file\"'"
    fi
done

# Create a temporary file to hold the response
tempfile=$(mktemp)

# Execute the command, write the body to the temporary file, and capture the status code separately
status_code=$(eval $command -w "%{http_code}" -o $tempfile)

# Read the body from the temporary file
body=$(cat $tempfile)

# Print the body
echo "$body"

# Check if the status code is not 200
if [ "$status_code" -ne 200 ]; then
    echo "Error: Received HTTP status code $status_code"
    exit 1
fi