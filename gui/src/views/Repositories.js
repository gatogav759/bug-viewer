import React, { useEffect, useState } from "react";
import {
  List,
  Card,
  Input,
  Pagination,
  Typography,
  Breadcrumb,
  Popconfirm,
  notification,
  Table,
  Space,
  Button,
} from "antd";
import {
  DeleteOutlined,
  BranchesOutlined,
} from "@ant-design/icons";
import axios from "axios";
import queryString from "query-string";
import { Content } from "antd/es/layout/layout";
import { createSearchParams, useNavigate } from "react-router-dom";

function Repositories() {
  const [loading, setLoading] = useState(true);
  const [repositories, setRepositories] = useState([]);
  const [page, setPage] = useState(null);
  const [total, setTotal] = useState({});
  const [name, setName] = useState(null);

  const [api, contextHolder] = notification.useNotification();
  const openNotificationWithIcon = (type, description) => {
    api[type]({
      message: type[0].toUpperCase() + type.slice(1),
      description,
    });
  };

  const navigate = useNavigate();

  useEffect(() => {
    getResults(name, 1);
  }, [name]);

  function getResults(name, page) {
    setLoading(true);
    let query = queryString.stringify({
      name,
      page,
    });

    axios
      .get("/api/repository?" + query, { withCredentials: true })
      .then((response) => {
        setRepositories(response.data.results.data);
        setTotal(response.data.results.total);
        setPage(response.data.page);
      })
      .finally(() => {
        setLoading(false);
      });
  }

  function removeRepository(repository) {
    let query = queryString.stringify({
      repository
    });
    axios
      .delete("/api/repository?" + query)
      .then(() => {
        openNotificationWithIcon("success", "Repository has been removed.");
        getResults(name, 1);
      })
      .catch(() => {
        openNotificationWithIcon("error", "Repository could not be removed.");
      });
  }

  return (
    <>
      <Typography.Title level={3}>
        <BranchesOutlined /> Repositories
      </Typography.Title>

      <Breadcrumb
        style={{ marginBottom: 20 }}
        items={[
          {
            title: "Home",
          },
          {
            title: "Repositories",
          },
        ]}
      />

      <Content style={{ backgroundColor: "white", padding: 20 }}>
        <Input.Search
          loading={loading}
          allowClear={true}
          placeholder="Search"
          onEmptied={() => {
            setName("");
          }}
          onSearch={(name) => {
            setName(name);
          }}
          style={{
            width: 305,
            marginBottom: 10,
          }}
        />

        <Table pagination={false} dataSource={repositories} columns={[
          {
            title: 'Repository name', dataIndex: 'repository', key: 'repository', width: '100%'
          },
          {
            title: 'Actions', key: 'Actions', render: (_, record) => (
              <Space>
                <Popconfirm
                    title="Remove repository"
                    description="Are you sure to remove this repository?"
                    onConfirm={() => {
                      removeRepository(record.repository);
                    }}
                    okText="Yes"
                    cancelText="No"
                  >
                  <Button type="link" danger icon={<DeleteOutlined/>}>Delete</Button>
                </Popconfirm>
                <Button type="link" icon={<BranchesOutlined/>} onClick={() => {
                      navigate({ pathname: 'branch', search: `?${queryString.stringify({ repository: record.repository})}` });
                    }}>View branches</Button>
              </Space>
            )
          }
        ]} />

        <div style={{ textAlign: "center", marginTop: 10 }}>
          <Pagination
            total={total}
            current={parseInt(page?.current) || 1}
            pageSize={20}
            showSizeChanger={false}
            disabled={loading}
            onChange={(e) => {
              getResults(name, e);
            }}
          />
        </div>
      </Content>
      {contextHolder}
    </>
  );
}

export default Repositories;
