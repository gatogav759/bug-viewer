# bug-viewer

The tool agregates results from different open source code scanners and display those results in a web interface. In a nutshell, it receives scanner results in JSON format and display them in a readable format. Our implementation of some popular scanners can be found under the scanner folder but feel free to use your implementation.

## Deployment

### Server
One easy way to deploy the server (API and Web Interface) is by using docker-compose.

Start by Cloning the project
```
git clone https://github.com/leandro-lorenzini/bug-viewer.git
```
Generate a self-signed certificate inside the project folder in case you don't have a valid ssl certificate yet.
```
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout certificate.key -out certificate.crt
```
Set the required environment variables in `docker-compose.yml`
|Variable           |Default Value                    |Required   |Description                                                   |
|-------------------|---------------------------------|----------|---------------------------------------------------------------|
|api.SESSION_SECRET |                                 |Yes       |A ramdom and complex value to be used by express-session       |
|nginx.SERVER_NAME  |                                 |Yes       |The server's fqdn or IP address, eg: bugviewer.domain.com      |
|api.MONGO_URI      |mongodb://mongo:27017/bug-viewer |No        |Set this variable if you want to use a different mongodb server|

Spin the containers
```
docker-compose up -d
```


If you didn't change the nginx ports, the server ui should be acessible under the url ```https://<SERVER_NAME>```, the default admin user and password is ```admin@localhost/admin@localhost```.

### Scanners
The scanners run on the client side. They scan the project and send the results to the server.

So far I only tested the scanner using github runners. An example can be found under [.github/workflows](tree/main/.github/workflows), there you can see two files:

- default.yml - This will trigger the scan to run for every pull request.
- periodic.yml - This will trigger a periodic scan of the main branch.

Those files can be used as example of how to set up the scanners in your repository. It should also work in other CI/CD, just make sure that the image running the scanner has bash installed and docker engine running in case you want to scan docker images.

A sample of how scan results should be sent to the server can be found [here](blob/main/scanner/submit.sh). The JSON parser is selected according to the uploaded file(s) name, make sure to include ```__ParserName__``` in the filename. Parsers can be viewd in https://<SERVER_NAME>/parser. You can use one of the existing parsers or create your own.
