#!/bin/bash

mkdir tmp
mkdir tmp/result
cd ..

if [ "$FULL_CHECK" = true ]; then
    tf=$(find . -name '*.tf' -type f -print -quit | grep -q . && echo true || echo false)
    k8=$(find . \( -name 'deployment.yaml' -o -name 'deployment.yml' -o -name 'deploy.yaml' -o -name 'deploy.yml' -o -name 'kustomization.yaml' -o -name 'kustomization.yml' \) -type f -print -quit | grep -q . && echo true || echo false)
    js=$(find . \( -name '*.js' -o -name '*.jsx' \) -type f -print -quit | grep -q . && echo true || echo false)
    py=$(find . -name '*.py' -type f -print -quit | grep -q . && echo true || echo false)
    go=$(find . -name '*.go' -type f -print -quit | grep -q . && echo true || echo false)
    docker=$(find . \( -name 'Dockerfile' -o -name 'docker-compose.*' \) -type f -print -quit | grep -q . && echo true || echo false)
else
    echo "Current Branch:"
    git rev-parse --abbrev-ref HEAD
    echo "All Branches:"
    git branch -a
    echo "Trying the diff:"
    git diff --name-only HEAD $(git merge-base HEAD remotes/origin/$BRANCH)

    tf=$(git diff --name-only HEAD $(git merge-base HEAD remotes/origin/$BRANCH) | grep -c '\.tf$' | grep -q '^0$' || echo true)
    k8=$(git diff --name-only HEAD $(git merge-base HEAD remotes/origin/$BRANCH) | egrep -c 'deployment.yaml$|deployment.yml$|deploy.yaml$|deploy.yml$|kustomization.yaml$|kustomization.yml$' | grep -q '^0$' || echo true)
    js=$(git diff --name-only HEAD $(git merge-base HEAD remotes/origin/$BRANCH) | grep -c '\.\(js\|jsx\)$' | grep -q '^0$' || echo true)
    py=$(git diff --name-only HEAD $(git merge-base HEAD remotes/origin/$BRANCH) | grep -c '\.py$' | grep -q '^0$' || echo true)
    go=$(git diff --name-only HEAD $(git merge-base HEAD remotes/origin/$BRANCH) | grep -c '\.go$' | grep -q '^0$' || echo true)

    export MODIFIED_FILES=$(git diff --name-only HEAD $(git merge-base HEAD remotes/origin/$BRANCH))
fi

# DOCKER IMAGE SCANNING
if [ "$docker" = "true" ]; then
    sh ./scanner/docker.sh
fi

# JAVASCRIPT SCANNING
if [ "$js" = "true" ]; then
    sh ./scanner/javascript.sh
fi

# PYTHON SCANNING
if [ "$py" = "true" ]; then
    sh ./scanner/python.sh
fi

# GOLANG SCANNING
if [ "$go" = "true" ]; then
    sh ./scanner/go.sh
fi

# TERRAFORM SCANNING
if [ "$tf" = "true" ]; then
    sh ./scanner/terraform.sh
fi

# KUBERNETES SCANNING
if [ "$k8" = "true" ]; then
    sh ./scanner/kubernetes.sh
fi

# HARDCODED PASSWORDS SCANNING
sh ./scanner/gitleaks.sh

# SUBMIT THE RESULT TO THE SERVER
sh ./scanner/submit.sh
