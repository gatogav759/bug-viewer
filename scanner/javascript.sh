#!/bin/bash

echo "Installing eslint"
npm i eslint eslint-plugin-security eslint-plugin-sonarjs eslint-plugin-xss eslint-plugin-no-unsanitized eslint-plugin-no-secrets eslint-plugin-react eslint-plugin-prettier
npm i @microsoft/eslint-formatter-sarif
# Look for package.json files in order to know where to run eslint
find . -mindepth 2 -type f -name "package.json" -not -path './node_modules/*' -exec dirname {} \; | while read -r dir; do

  echo "Checking directory $dir"

  import_count=0
  require_count=0

  # Check and count import and require statements in .js, .jsx, and .ts files
  import_count=$(find "$dir" -type f \( -name "*.js" -o -name "*.jsx" -o -name "*.ts" \) -exec grep -E 'import .* from .*' {} \; | wc -l)
  require_count=$(find "$dir" -type f \( -name "*.js" -o -name "*.jsx" -o -name "*.ts" \) -exec grep -E 'require\(".*"\)' {} \; | wc -l)

  echo "Number of import statements: $import_count"
  echo "Number of require statements: $require_count"

  # Determine the folder name for the output file
  foldername=$(basename "$dir")

  # Assign ESLint config path based on the condition
  if [ $import_count -gt $require_count ]; then
    eslint_config="scanner/.eslintrc-import"
  else
    eslint_config="scanner/.eslintrc-require"
  fi

  if [ "$FULL_CHECK" = true ]; then
    echo "Running eslint with configuration $eslint_config in $dir"
    fileNumber=$RANDOM
    find "$dir" -name 'package.json' -type f -exec rm -f {} +
    (npx eslint "$dir" --config "$eslint_config" --format json > "./scanner/tmp/result/__eslint__${fileNumber}.json")
  else
    modified_files=$(git diff --name-only HEAD $(git merge-base HEAD remotes/origin/$BRANCH) | grep -E '\.(js|jsx|ts)$')
    echo "Modified files: $modified_files"

    # Check if any modified file is under the current directory
    for file in $modified_files; do
      echo "$file = $dir"
      if [[ "./$file" == "$dir"* ]]; then
        echo "Running eslint with configuration $eslint_config in $dir for file ./$file"
        fileNumber=$RANDOM
        (npx eslint "$file" --config "$eslint_config" --format json > "./scanner/tmp/result/__eslint__$fileNumber.json")
      fi
    done
  fi
done
