FROM node:20 as build
WORKDIR /usr/src/app
COPY ./gui/package*.json ./
RUN npm install
COPY ./gui .
RUN npm run build

FROM nginx:alpine
COPY --from=build /usr/src/app/build /usr/share/nginx/html

EXPOSE 443
CMD ["nginx", "-g", "daemon off;"]

CMD /bin/sh -c "envsubst '\$SERVER_NAME' < /etc/nginx/nginx.conf.template > /etc/nginx/nginx.conf && exec nginx -g 'daemon off;'"

