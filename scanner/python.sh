#!/bin/bash

echo "Installing bandit"
pip install bandit

echo "Scanning Python files"
if [ "$FULL_CHECK" = true ]; then
    bandit -r . -f json > ./scanner/tmp/result/__badit__.json
else
    bandit -r $(git diff --name-only HEAD $(git merge-base HEAD remotes/origin/$BRANCH) | grep '\.py$') -f json > ./scanner/tmp/result/__badit__.json
fi
