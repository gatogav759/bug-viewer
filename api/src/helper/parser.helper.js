const zlib = require("zlib");

function parse(parser, data, removePath) {
    try {
        
        var results = parser.rootPath?.length ? data[parser.rootPath] : data;
        
        // Unwinding array
        if (parser.unwind?.length) {
            let newResults = [];
            for (let result of results) {
                result = result[parser.unwind].map(item => {
                    for (let key of Object.keys(result)) {
                        if (key !== parser.unwind) {
                            item[key] = result[key];
                        }
                    }
                    return item;
                });
                newResults = newResults.concat(result)
            }
            results = newResults;
        }
        
        // Mapping result fields
        results = results.map((result) => {
            let newResult = {};
            for (let key of Object.keys(parser.fields)) {
                if (parser.fields[key]?.length && parser.fields[key].includes('"')) {
                    // Hardcoded value
                    newResult[key] = parser.fields[key].replace('"',"").replace('"',"");
                } else {
                    if (parser.fields[key]?.length && parser.fields[key].includes(".")) {
                        if (parser.fields[key][0] === "/") {
                            newResult[key] = parser.fields[key].replace("/", "").split('.').reduce((o, key) => (o && o[key] ? o[key] : null), data);    
                        } else {
                            newResult[key] = parser.fields[key].split('.').reduce((o, key) => (o && o[key] ? o[key] : null), result);
                        }
                    } else {
                        // Value under the root path
                        if(result[parser.fields[key]]) {
                            if (parser.fields[key][0] === "/") {
                                newResult[key] = data[parser.fields[key].replace("/","")];
                            } else {
                                newResult[key] = result[parser.fields[key]];
                            }
                        }
                    }
                }
            }
            newResult['provider'] = parser.name;
            
            switch(String(newResult['severity'])) {
                case String(parser.severities.critical):
                    newResult['severity'] = 'CRITICAL'
                    break;
                case String(parser.severities.medium):
                    newResult['severity'] = 'MEDIUM'
                    break;
                case String(parser.severities.low):
                    newResult['severity'] = 'LOW'
                    break;
                default:
                    newResult['severity'] = 'HIGH'
            }

            if (newResult['file']) {
                if (removePath) {
                    newResult['file'] = newResult['file'].replace(removePath, "");
                }
                if (newResult['file'][0] === "/") {
                    newResult['file'] = newResult['file'].substring(1);
                }
            }

            newResult['details'] = zlib.gzipSync(JSON.stringify(result)).toString("base64");
            return newResult;
        })
        return results;
    } catch (error) {
        console.log(error);
        return false;
    }
}

module.exports = parse;
