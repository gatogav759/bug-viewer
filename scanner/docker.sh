#!/bin/bash

echo "Installing grype"
curl -sSfL https://raw.githubusercontent.com/anchore/grype/main/install.sh | sh -s -- -b /usr/local/bin

echo "Building images"

# Search and build images from Dockerfiles if they have been modified
find . -name 'Dockerfile' -exec sh -c '
for file do
    DIR=$(dirname "$file")
    TAG_NAME=$(echo "$file" | sed "s/^\.\///g" | sed "s/\//__SLASH__/g" | tr '[:upper:]' '[:lower:]')
    if [ "$FULL_CHECK" = true ] || git diff --name-only HEAD $(git merge-base HEAD '"$BRANCH"') | grep -E "Dockerfile$" | grep -Fxq "$file"; then
        echo "Building Docker image in directory: $DIR"
        docker build -t "$TAG_NAME" "$DIR"
    fi
done
' sh {} +

# Search and build images from docker-compose files with .yml or .yaml extension if they have been modified
find . \( -name 'docker-compose.yml' -o -name 'docker-compose.yaml' \) -exec sh -c '
for file do
    if [ "$FULL_CHECK" = "true" ] || git diff --name-only HEAD $(git merge-base HEAD '"$BRANCH"') | grep -E "docker-compose\.ya?ml$" | grep -Fxq "$file"; then
        echo "Building Docker images defined in docker-compose file: $file"
        docker-compose -f "$file" up -d --build
    fi
done
' sh {} +

# Get the images
images=$(docker images --format "{{.Repository}}:{{.Tag}}")

echo "Creating a local registry"
docker run -d -p 5000:5000 --restart=always --name registry registry:2

# Loop through each image
for image in $images; do
    case "$image" in
        *dockerfile*|*docker-compose*)
            # Create a valid name for the JSON file
            filename=$(echo "$image" | tr -d '/:' | tr '[:upper:]' '[:lower:]')
            
            # Tag the image
            docker tag $image localhost:5000/$image
            # Push the image to the local registry
            docker push localhost:5000/$image
            # Scan the image with Grype and output results to JSON file
            echo "Scanning $image"
            grype localhost:5000/$image -o json --file ./scanner/tmp/result/__grype__$filename.json

            ;;
    esac
done
