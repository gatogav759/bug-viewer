const models = require("./models");
const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;

function repositories(name, skip) {
  return new Promise((resolve, reject) => {
    let filter = !name
      ? null
      : {
          repository: {
            $regex: name,
            $options: "i",
          },
        };

    const pipeline = filter ? [{ $match: filter }] : [];
    pipeline.push(
      { $group: { _id: "$repository" } },
      { $sort: { _id: 1 } },
      { $project: {
        _id: 0,
        repository: '$_id'
      }},
      { $skip: skip || 0 },
      { $limit: 20 }
    );

    const pipelineTotal = filter ? [{ $match: filter }] : [];
    pipelineTotal.push(
      { $group: { _id: "$repository" } },
    );

    Promise.all([
      models.branch.aggregate(pipeline),
      models.branch.aggregate(pipelineTotal),
    ])  
      .then((values) => {
        resolve({
          repositories: values[0],
          total: values[1]?.length || 0,
        });
      })
      .catch((error) => {
        reject(error);
      });
  });
}

function branches(repository, ref, skip) {
  return new Promise((resolve, reject) => {

    var filter = !ref ? 
      { repository } : 
      {
        repository,
        ref: {
          $regex: ref,
          $options: "i",
        }
      };

    Promise.all([
      models.branch.find(filter, { findings: 0 }).skip(skip||0).limit(20),
      models.branch.find(filter),
    ])
      .then((values) => {
        resolve({
          branches: values[0],
          total: values[1].length,
        });
      })
      .catch((error) => {
        reject(error);
      });
  });
}

function upsert(repository, ref, findings) {
  return new Promise((resolve, reject) => {
    models.branch.findOne({ repository, ref }).then(async (doc) => {

      if (doc) {
        console.log('Deleting current branch scan')
        await models.branch.deleteOne({ _id: doc._id });
      }

      let branch = new models.branch({
        repository,
        ref,
        findings
      });

      branch.save()
        .then((doc) => {
          resolve(doc._id);
        })
        .catch((error) => {
          reject(error);
        });
    });
  });
}

function findings(
  branchId,
  providers,
  ruleIds,
  severities,
  files,
  skip
) {
  return new Promise((resolve, reject) => {
    let matchConditions = {};

    if (providers) matchConditions["provider"] = { $in: providers };
    if (ruleIds) matchConditions["ruleId"] = { $in: ruleIds };
    if (severities) matchConditions["severity"] = { $in: severities };
    if (files) matchConditions["file"] = { $in: files };

    console.log(matchConditions);

    let aggregationFindings = [
      { $match: { _id: new ObjectId(branchId) } },
      { $unwind: "$findings" },
      { $replaceWith: "$findings" },
    ];

    let aggregationTotal = [
      { $match: { _id: new ObjectId(branchId) } },
      { $unwind: "$findings" },
      { $replaceWith: "$findings" },
    ];

    if (Object.keys(matchConditions).length > 0) {
      aggregationFindings.push({ $match: matchConditions });
      aggregationTotal.push({ $match: matchConditions });
    }

    aggregationFindings.push(
      { $sort: { severity: 1, _id: 1 } },
      { $skip: skip || 0 },
      { $limit: 20 }
    );

    aggregationTotal.push(
      { $group: { _id: null, count: { $sum: 1 } } },
      {
        $project: {
          total: "$count",
        },
      }
    );

    let aggregationProperties = [
      { $match: { _id: new ObjectId(branchId) } },
      { $unwind: "$findings" },
      {
        $group: {
          _id: null,
          providers: { $addToSet: "$findings.provider" },
          ruleIds: { $addToSet: "$findings.ruleId" },
          files: { $addToSet: "$findings.file" },
          severities: { $addToSet: "$findings.severity" },
        },
      },
      {
        $project: {
          ruleIds: 1,
          files: 1,
          providers: 1,
          severities: 1,
          _id: 0,
        },
      },
    ];

    Promise.all([
      models.branch.aggregate(aggregationFindings),
      models.branch.aggregate(aggregationProperties),
      models.branch.aggregate(aggregationTotal),
    ])
      .then((values) => {
        resolve({
          findings: values[0],
          properties: values[1][0],
          total: values[2][0]?.total || 0,
        });
      })
      .catch((error) => {
        reject(error);
      });
  });
}

function removeRepository(repository) {
  return new Promise((resolve, reject) => {
    models.branch
      .deleteMany({ repository })
      .then((result) => {
        if (result.deletedCount) {
          resolve();
        } else {
          reject();
        }
      })
      .catch((error) => {
        reject(error);
      });
  });
}

function removeBranch(branchId) {
  return new Promise((resolve, reject) => {
    models.branch
      .deleteOne({ _id: branchId })
      .then((result) => {
        if (result.deletedCount) {
          resolve();
        } else {
          reject();
        }
      })
      .catch((error) => {
        reject(error);
      });
  });
}

module.exports = {
  upsert,
  findings,
  repositories,
  branches,
  removeRepository,
  removeBranch,
};
